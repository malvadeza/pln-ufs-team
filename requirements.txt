Django==1.8.4
pytz==2015.4
python-dateutil==2.4.2
cchardet==1.0.0
nltk==3.1
tweepy==3.4.0
