# -*- coding: utf-8 -*-

from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.http.response import HttpResponse

from django.contrib.auth.models import User
from django.core.validators import validate_email

from django.shortcuts import render, redirect


def novo_usuario(request):
    return render(request, "WebCrawlerEngine/novo_usuario.html")

@login_required(login_url='/login/')
def editar_dados_usuario(request):
    if request.method == "GET":
        return render(request, "WebCrawlerEngine/editar_dados_usuario.html", {"user": request.user})

    nome = request.POST["nome"].strip()
    sobrenome = request.POST["sobrenome"].strip()
    email = request.POST["email"].strip()
    senha = request.POST["senha"].strip()
    confirmacao_senha = request.POST["confirmacao_senha"].strip()
    user = request.user

    try:
        email = validate_email(email)
    except ValidationError:
        context = {"erro": "Invalid email"}
        return render(request, "WebCrawlerEngine/editar_dados_usuario.html", context)
    if "" in (nome, sobrenome):
        context = {"erro": "Please, Enter new name and new lastname"}
        return render(request, "WebCrawlerEngine/editar_dados_usuario.html", context)
    else:
        user.first_name = nome
        user.last_name = sobrenome
    if senha and senha <> confirmacao_senha:
        context = {"erro": "Passwords do not match"}
        return render(request, "WebCrawlerEngine/editar_dados_usuario.html", context)
    elif senha:
        user.set_password(senha)
    user.save()
    context = {"info": "Dados salvos com sucesso", "user": request.user}
    return render(request, "WebCrawlerEngine/editar_dados_usuario.html", context)





def cadastrar_novo_usuario(request):
    nome = request.POST["nome"].strip()
    sobrenome = request.POST["sobrenome"].strip()
    email = request.POST["email"].strip()
    senha = request.POST["senha"].strip()
    confirmacao_senha = request.POST["confirmacao_senha"].strip()

    if ("" in (nome, sobrenome, email, senha, confirmacao_senha)):
        context = {"erro": "Todos os campos são obrigatórios"}
        return render(request, "WebCrawlerEngine/novo_usuario.html", context)
    elif confirmacao_senha <> senha:
        context = {"erro": "Senhas não conferem"}
        return render(request, "WebCrawlerEngine/novo_usuario.html", context)
    elif User.objects.filter(email=email).exists():
        context = {"erro": "Email já cadastrado na base de dados"}
        return render(request, "WebCrawlerEngine/novo_usuario.html", context)
    else:
        user = User.objects.create_user(username=email, first_name=nome, last_name=sobrenome, email=email, password=senha)
        context = {"info": "Usuário cadastrado com sucesso"}

    return render(request, "WebCrawlerEngine/login.html", context)


def login(request):
    try:
        email = request.POST.get(u'email').lower()
        user = authenticate(username=email, password=request.POST.get(u'senha'))
        if user is not None:
            django_login(request, user)
            next = request.POST.get("next") or "WebCrawler:index"
            return redirect(next)
        else:
            context = {"erro": "Invalid e-mail or password."}
            return render(request, "WebCrawlerEngine/login.html", context)
    except AttributeError:
            context = {"erro": "Enter login.",  "next": request.GET.get("next","")}
            return render(request, "WebCrawlerEngine/login.html", context)


def logout(request):
    django_logout(request)
    context = {"info": "Successfully logout"}
    return redirect("login")
