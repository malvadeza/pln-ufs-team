/**
 * Created by OI on 28/06/2015.
 */

function enable(arrayid){
    for (i = 0; i < arrayid.length; i++ ){
        document.getElementById(arrayid[i]).disabled = false;
    }
}

function disable(arrayid){
    for (i = 0; i < arrayid.length; i++ ){
        document.getElementById(arrayid[i]).disabled = true;
    }
}

function disableAll(idcheckbox, id){
    if (document.getElementById(idcheckbox).checked = true) {
        var nodes = document.getElementById(id).getElementsByTagName('*');
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].disabled = true;
        }
    }else{
        var nodes = document.getElementById(id).getElementsByTagName('*');
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].disabled = false;
        }
    }
}

 function ativaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
 }


function selecionarTodosOsDominios(){
	habilitar = document.getElementById('checkbox_todos').checked;
	if (habilitar){
	    $('#slc_dominios').multiSelect('select_all');
	}else{
	    $('#slc_dominios').multiSelect('deselect_all');
	}

}

function exemplos(tipo) {
    switch (document.getElementById('formato_saida_entidades').value) {
        case 'slashtags':
            document.getElementById('exemplo').innerHTML = 'Exemplo: '+'José/Pessoa gosta/O de/O chocolate/O';
            break;
        case 'TSV':
            document.getElementById('exemplo').innerHTML = 'Exemplo: '+'João&emsp;&emsp;Pessoa <br/> &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; chorou&emsp;&emsp;O';
            break;
        case 'tabbedEntities':
            document.getElementById('exemplo').innerHTML = 'Exemplo: '+'&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A <br/> Google &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' +
                                                                       'ORGANIZAÇÃO&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; está &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' +
                                                                       '&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  revolucionando a computação';
            break;
        case 'XML':
            document.getElementById('exemplo').innerHTML = 'Exemplo: '+'&lt;wi num="0" entity="Pessoa"&gt;Maria&lt;/wi&gt; <br/> &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' +
                                                                        ' &lt;wi num="1" entity="O"&gt;está&lt;/wi&gt; <br/> &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' +
                                                                        '&lt;wi num="2" entity="O"&gt;dormindo&lt;/wi&gt;';
            break;
        case 'inlineXML':
            document.getElementById('exemplo').innerHTML = 'Exemplo: '+'O lugar mais alto do mundo é o <br/> &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                                                                       '&lt;Local&gt;Monte Everest&lt;/Local&gt;';
            break;
    }
}
function habilitarDominios(habilitar){
    if (habilitar){
        $('#slc_dominios').removeAttr('disabled');
    }else{
        $('#slc_dominios').attr('disabled', 'disabled');
    }
}

function habilitarBotaoArquivoPerplexidade(){
		if (document.getElementById("check_carregar_arquivo_perplexidade").checked){
		$("#file_carregar_arquivo_perplexidade").filestyle('disabled', false);
//			document.getElementById("file_carregar_arquivo_perplexidade").removeAttribute("disabled");
        }else{
        $("#file_carregar_arquivo_perplexidade").filestyle('disabled', true);
//            document.getElementById("file_carregar_arquivo_perplexidade").disabled = true;
        }
}
function habilitarFontesDeColetaParaAbrangencia(abrangencia){
	if (abrangencia == "web"){
		 document.getElementById("radio_urls_sementes").removeAttribute("disabled");
		 document.getElementById("radio_coleta_arquivo").removeAttribute("disabled");
		 document.getElementById("radio_palavras_chave").removeAttribute("disabled");
		 $("#arquivo_dados_acesso_twitter").filestyle('disabled', true);
		 fonte_coleta = $('input[name=fonte_coleta]:radio:checked').val()
		 if (fonte_coleta != "arquivo_urls"){
		    document.getElementById("coleta_select_file").disabled = true;
		 }else{
		    document.getElementById("coleta_select_file").removeAttribute("disabled");
		 }
	}else{
		 document.getElementById("radio_urls_sementes").disabled = true;
		 document.getElementById("radio_coleta_arquivo").disabled = true;
		 document.getElementById("radio_palavras_chave").checked = true;
		 document.getElementById("coleta_select_file").disabled = true;
	}
}

function tratarFonteColetaUrlsSementes(){
    qtd = $("#slc_dominios :selected").length;
    if (fonte_coleta == "urls_sementes"){
	    if(qtd > 0){
	         desabilitaLabel();
	         return true;
	    }
	    habilitaLabel();
    }else{
        desabilitaLabel();
        return false;
    }
}

function habilitaLabel(){
    document.getElementById("msg_urls_sementes").style.visibility = "visible";
}

function desabilitaLabel(){
    document.getElementById("msg_urls_sementes").style.visibility = "hidden";
}

function validarTodosCamposESubmeterForm(){
    if (document.getElementById("radio_abrangencia_web").checked) {
		if ($('input[name=fonte_coleta]:radio:checked').length > 0) {
			fonte_coleta = $('input[name=fonte_coleta]:radio:checked').val()
			if (fonte_coleta == 'urls_sementes') {
                if (!tratarFonteColetaUrlsSementes()){
					alert("Escolha o domínio.");
					ativaTab("aba_preprocessamento");
					return
                }
			}else if (fonte_coleta == "arquivo_urls"){
				if (document.getElementById("coleta_select_file").value.length == 0){
					alert("Informe o arquivo de urls sementes.");
					ativaTab("aba_preprocessamento");
					return
				}
			} else {
				if (document.getElementById("text_palavras_chave").value.length == 0){
					alert("Informe as palavras chave para pesquisa.");
					ativaTab("aba_preprocessamento");
					return
				}
			}
			qtd_palavras = $.trim(document.getElementById("input_qtd_palavras").value);
			if (qtd_palavras.length > 0) {
				if (isNaN(qtd_palavras)){
					alert("Informe somente números no campo de quantidade de palavras.");
					ativaTab("aba_preprocessamento");
					return
				}
			}
		} else {
			alert("Escolha a Fonte de Coleta");
			ativaTab("aba_preprocessamento");
			return
		}
    } else if (document.getElementById("radio_abrangencia_twitter").checked) {
		if (document.getElementById("arquivo_dados_acesso_twitter").value.length == 0){
				alert("Informe o arquivo com os dados de acesso ao twitter.");
				ativaTab("aba_preprocessamento");
				return
		}
	} else{
		alert("Escolha a Abrangência.");
		ativaTab("aba_preprocessamento");
		return
	}
	document.getElementById("frmPrincipal").submit();
}